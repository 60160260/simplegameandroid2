package buu.yosapron.simplegameandroid2

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import buu.yosapron.simplegameandroid2.databinding.FragmentTitleBinding

class TitleFragment : Fragment() {

    private var amountCorrect = 0
    private var amountIncorrect = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentTitleBinding>(
            inflater,
            R.layout.fragment_title, container, false
        )
        val args = TitleFragmentArgs.fromBundle(arguments!!)

        amountCorrect = args.amountCorrect
        amountIncorrect = args.amountIncorrect

        binding.ansCorrect.text = amountCorrect.toString()
        binding.ansIncorrect.text = amountIncorrect.toString()

        binding.btnPlus.setOnClickListener { view ->

            val set = TitleFragmentDirections.actionTitleFragmentToGamePlus()
            set.amountCorrect = amountCorrect
            set.amountIncorrect = amountIncorrect
            view.findNavController()
                .navigate(set)
        }

        binding.btnMinus.setOnClickListener { view ->

            val set = TitleFragmentDirections.actionTitleFragmentToGameMinus()
            set.amountCorrect = amountCorrect
            set.amountIncorrect = amountIncorrect
            view.findNavController()
                .navigate(set)
        }

        binding.btnMultiply.setOnClickListener { view ->

            val set = TitleFragmentDirections.actionTitleFragmentToGameMultiply()
            set.amountCorrect = amountCorrect
            set.amountIncorrect = amountIncorrect
            view.findNavController()
                .navigate(set)
        }

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.option_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item, view!!.findNavController())
                || super.onOptionsItemSelected(item)
    }

}