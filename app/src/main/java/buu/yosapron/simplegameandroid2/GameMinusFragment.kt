package buu.yosapron.simplegameandroid2

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import buu.yosapron.simplegameandroid2.databinding.FragmentGameMinusBinding
import kotlin.random.Random

class GameMinusFragment : Fragment() {
    private lateinit var binding: FragmentGameMinusBinding
    private var amountCorrect = 0
    private var amountIncorrect = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_game_minus, container, false)

        val args = GameMinusFragmentArgs.fromBundle(arguments!!)
        
        amountCorrect = args.amountCorrect
        amountIncorrect = args.amountIncorrect

        binding.ansCorrect.text = args.amountCorrect.toString()
        binding.ansIncorrect.text = args.amountIncorrect.toString()

        play()

        binding.btnBack.setOnClickListener { view ->
            val set = GameMinusFragmentDirections.actionGameMinusFragmentToTitleFragment()
            set.amountCorrect = amountCorrect
            set.amountIncorrect = amountIncorrect
            view.findNavController()
                .navigate(set)
        }

        return binding.root
    }

    private fun play() {


        val textNum1 = binding.textNum1
        val textNum2 = binding.textNum2

        val btnChoose1 = binding.btnChoose1
        val btnChoose2 = binding.btnChoose2
        val btnChoose3 = binding.btnChoose3

        btnChoose1.setBackgroundColor(Color.GRAY)
        btnChoose2.setBackgroundColor(Color.GRAY)
        btnChoose3.setBackgroundColor(Color.GRAY)
        btnChoose1.isEnabled = true
        btnChoose2.isEnabled = true
        btnChoose3.isEnabled = true

        val textSelect = binding.textSelect
        val ansCorrect = binding.ansCorrect
        val ansIncorrect = binding.ansIncorrect


        val pair = randomNumAndSetText(textNum1, textNum2)
        val randomPattern = pair.first
        val ans = pair.second

        if (randomPattern == 1) {

            btnChoose1.text = "${ans}"
            btnChoose2.text = "${ans - 1}"
            btnChoose3.text = "${ans + 1}"

            btnChoose1.setOnClickListener {
                textSelect.text = "ถูกต้อง"
                ansCorrect.text = "${++amountCorrect}"
                randomNumAndSetText(textNum1, textNum2)
                play()
            }
            btnChoose2.setOnClickListener {
                textSelect.text = "ไม่ถูกต้อง"
                ansIncorrect.text = "${++amountIncorrect}"
                btnChoose2.setBackgroundColor(Color.RED)
                btnChoose2.isEnabled = false
            }
            btnChoose3.setOnClickListener {
                textSelect.text = "ไม่ถูกต้อง"
                ansIncorrect.text = "${++amountIncorrect}"
                btnChoose3.setBackgroundColor(Color.RED)
                btnChoose3.isEnabled = false
            }

        } else if (randomPattern == 2) {

            btnChoose1.text = "${ans + 1}"
            btnChoose2.text = "${ans}"
            btnChoose3.text = "${ans - 1}"

            btnChoose1.setOnClickListener {
                textSelect.text = "ไม่ถูกต้อง"
                ansIncorrect.text = "${++amountIncorrect}"
                btnChoose1.setBackgroundColor(Color.RED)
                btnChoose1.isEnabled = false
            }
            btnChoose2.setOnClickListener {
                textSelect.text = "ถูกต้อง"
                ansCorrect.text = "${++amountCorrect}"
                randomNumAndSetText(textNum1, textNum2)
                play()
            }
            btnChoose3.setOnClickListener {
                textSelect.text = "ไม่ถูกต้อง"
                ansIncorrect.text = "${++amountIncorrect}"
                btnChoose3.setBackgroundColor(Color.RED)
                btnChoose3.isEnabled = false
            }
        } else if (randomPattern == 3) {

            btnChoose1.text = "${ans - 2}"
            btnChoose2.text = "${ans + 2}"
            btnChoose3.text = "${ans}"

            btnChoose1.setOnClickListener {
                textSelect.text = "ไม่ถูกต้อง"
                ansIncorrect.text = "${++amountIncorrect}"
                btnChoose1.setBackgroundColor(Color.RED)
                btnChoose1.isEnabled = false
            }
            btnChoose2.setOnClickListener {
                textSelect.text = "ไม่ถูกต้อง"
                ansIncorrect.text = "${++amountIncorrect}"
                btnChoose2.setBackgroundColor(Color.RED)
                btnChoose2.isEnabled = false
            }
            btnChoose3.setOnClickListener {
                textSelect.text = "ถูกต้อง"
                ansCorrect.text = "${++amountCorrect}"
                randomNumAndSetText(textNum1, textNum2)
                play()
            }
        } else if (randomPattern == 4) {

            btnChoose1.text = "${ans}"
            btnChoose2.text = "${ans - 2}"
            btnChoose3.text = "${ans + 2}"

            btnChoose1.setOnClickListener {
                textSelect.text = "ถูกต้อง"
                ansCorrect.text = "${++amountCorrect}"
                randomNumAndSetText(textNum1, textNum2)
                play()
            }
            btnChoose2.setOnClickListener {
                textSelect.text = "ไม่ถูกต้อง"
                ansIncorrect.text = "${++amountIncorrect}"
                btnChoose2.setBackgroundColor(Color.RED)
                btnChoose2.isEnabled = false
            }
            btnChoose3.setOnClickListener {
                textSelect.text = "ไม่ถูกต้อง"
                ansIncorrect.text = "${++amountIncorrect}"
                btnChoose3.setBackgroundColor(Color.RED)
                btnChoose3.isEnabled = false
            }
        } else {

            btnChoose1.text = "${ans + 1}"
            btnChoose2.text = "${ans}"
            btnChoose3.text = "${ans + 2}"

            btnChoose1.setOnClickListener {
                textSelect.text = "ไม่ถูกต้อง"
                ansIncorrect.text = "${++amountIncorrect}"
                btnChoose3.setBackgroundColor(Color.RED)
                btnChoose3.isEnabled = false
            }
            btnChoose2.setOnClickListener {
                textSelect.text = "ถูกต้อง"
                ansCorrect.text = "${++amountCorrect}"
                randomNumAndSetText(textNum1, textNum2)
                play()
            }
            btnChoose3.setOnClickListener {
                textSelect.text = "ไม่ถูกต้อง"
                ansIncorrect.text = "${++amountIncorrect}"
                btnChoose3.setBackgroundColor(Color.RED)
                btnChoose3.isEnabled = false
            }
        }
    }

    private fun randomNumAndSetText(
        textNum1: TextView,
        textNum2: TextView
    ): Pair<Int, Int> {
        val randomTextNum1 = Random.nextInt(1, 10)
        val randomTextNum2 = Random.nextInt(1, 10)
        val randomPattern = Random.nextInt(1, 5)
        textNum1.text = "$randomTextNum1"
        textNum2.text = "$randomTextNum2"
        val ans = randomTextNum1 - randomTextNum2
        return Pair(randomPattern, ans)
    }

}